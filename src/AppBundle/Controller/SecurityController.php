<?php
/**
 * Created by PhpStorm.
 * User: Prog1
 * Date: 14.01.2016
 * Time: 15:24
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\Controller\SecurityController as BaseController;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends BaseController
{

    public function loginAction(Request $request)
    {
        if ($this->container->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            // redirect authenticated users to homepage
            return $this->redirect($this->generateUrl('app_homepage'));
        }

        /** @var $session \Symfony\Component\HttpFoundation\Session\Session */
        $session = $request->getSession();

        if (class_exists('\Symfony\Component\Security\Core\Security')) {
            $authErrorKey = Security::AUTHENTICATION_ERROR;
            $lastUsernameKey = Security::LAST_USERNAME;
        } else {
            // BC for SF < 2.6
            $authErrorKey = SecurityContextInterface::AUTHENTICATION_ERROR;
            $lastUsernameKey = SecurityContextInterface::LAST_USERNAME;
        }

        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey)) {
            $error = $request->attributes->get($authErrorKey);
        } elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        } else {
            $error = null;
        }

        if (!$error instanceof AuthenticationException) {
            $error = null; // The value does not come from the security component.
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);

        if ($this->has('security.csrf.token_manager')) {
            $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
        } else {
            // BC for SF < 2.4
            $csrfToken = $this->has('form.csrf_provider')
                ? $this->get('form.csrf_provider')->generateCsrfToken('authenticate')
                : null;
        }

        return $this->renderLogin(array(
            'last_username' => $lastUsername,
            'error' => $error,
            'csrf_token' => $csrfToken,
        ));
    }

    public function changeLanguageAction($changeToLocale)
    {
        $supportedLocale = $this->container->getParameter('locale_supported');
        if(!in_array($changeToLocale, $supportedLocale)){
            return false;
        }
        $router = $this->get('router');
        // Create URL path to pass it to matcher
        $request = $this->get('request');
        $referer = $request->headers->get('referer');
        $urlParts = parse_url($referer);
        $basePath = $request->getBaseUrl();
        $path = str_replace($basePath, '', $urlParts['path']);
        if ("" === $path) {
            $path = '/' . $changeToLocale;
        }
        $route = $router->match($path);
        $routeAttrs = array_replace($route, array('_locale' => $changeToLocale));
        $routeName = $routeAttrs['_route'];
        unset($routeAttrs['_route']);
        $cookie = new Cookie('_locale', $changeToLocale, (time() + 3600 * 24 * 7), '/');
        $response = new Response();
        $response->headers->setCookie($cookie);
        $response->send();
        return new RedirectResponse($router->generate($routeName, $routeAttrs));
    }
}