<?php
/**
 * Created by PhpStorm.
 * User: Prog1
 * Date: 14.01.2016
 * Time: 13:47
 */
namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class HomeController extends Controller{

    public function homeAction()
    {

        $user = $this->getUser();
//        $user->getAvatars()[0]->getPath();
//        dump($user->getAvatars()[0]->getPath());exit;
       return $this->render('home/home.html.twig', array('count' => "Hello world"));
    }


    public function apiTestAction()
    {
        $data = array(
            'lucky_number11' => rand(0, 100),
        );

        return new JsonResponse($data);
    }
}