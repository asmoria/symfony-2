<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
//    /**
//     * @Route("/", name="homepage")
//     */
    public function indexAction(Request $request)
    {
        // $this->container->getParameter('lifetime')
//        dump($request->getSession()->getMetadataBag()->getCreated());exit;
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir') . '/..'),
        ));
    }
//
//    public function localeAction($route = 'home', $parameters = array())
//    {
//        $this->getRequest()->setLocale($this->getRequest()->getPreferredLanguage(array('en', 'ua')));
//
//        return $this->redirect($this->generateUrl($route, $parameters));
//    }
}
